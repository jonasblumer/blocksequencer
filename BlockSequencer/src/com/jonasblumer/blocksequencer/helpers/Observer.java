package com.jonasblumer.blocksequencer.helpers;

public interface Observer {
	public void update(EventTypes updateType, double value, int target);
}
