package com.jonasblumer.blocksequencer.helpers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javafx.stage.FileChooser;

public class JsonIO {
	
	public static void writeJSON(JSONObject obj){
		
		FileChooser fileChooser = new FileChooser();
		File file = fileChooser.showOpenDialog(null);
		fileChooser.setTitle("Save Color Configs File");
		System.out.print(obj);
		try {

			FileWriter writer = new FileWriter(file.getPath());
			writer.write(obj.toJSONString());
			writer.flush();
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Object readJSON(){
		FileChooser fileChooser = new FileChooser();
		File file = fileChooser.showOpenDialog(null);
		fileChooser.setTitle("Load Color Configs File");
		
		if(file != null){
			JSONParser parser = new JSONParser();
			
			try {

				Object obj = parser.parse(new FileReader(file.getPath()));

				return obj;

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		
		
		return null;
	}
}
