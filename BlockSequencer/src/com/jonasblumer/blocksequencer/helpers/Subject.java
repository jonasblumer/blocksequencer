package com.jonasblumer.blocksequencer.helpers;

public interface Subject {
	public void registerObserver(Observer observer);
    public void removeObserver(Observer observer);
    public void notifyObservers(EventTypes updateType, double value, int target);
}
