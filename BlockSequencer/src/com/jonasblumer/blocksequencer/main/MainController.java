package com.jonasblumer.blocksequencer.main;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.opencv.core.Scalar;

import com.jonasblumer.blocksequencer.input.blockImage.calibration.CalibrationController;
import com.jonasblumer.blocksequencer.input.blockImage.opencv.OpenCVEditorController;
import com.jonasblumer.blocksequencer.input.blockImage.opencv.OpenCVSequencerController;
import com.jonasblumer.blocksequencer.output.audio.audioplayer.AudioPlayerController;
import com.jonasblumer.blocksequencer.output.audio.track.TrackFXController;
import com.jonasblumer.blocksequencer.output.audio.transport.TransportController;

import javafx.fxml.FXML;

public class MainController{
	

	
	private AudioPlayerController audioPlayerController;
	@FXML private OpenCVSequencerController openCVSequencerController;
	@FXML private OpenCVEditorController openCVEditorController;
	@FXML private TransportController transportController;
	@FXML private TrackFXController trackFX1Controller, trackFX2Controller, trackFX3Controller, trackFX4Controller;
	@FXML private CalibrationController calibrationController;
	
	// a timer for acquiring the video stream
	private ScheduledExecutorService timer;

	/*
	 * initializable needs not be implemented as JavaFX knows how to init a controller
	 * if inizialize() method is included (when properly annotated)
	 */
	@FXML public void initialize(){

		openCVSequencerController.init();

		openCVEditorController.init();

		audioPlayerController = new AudioPlayerController();
		audioPlayerController.setSequence(openCVSequencerController.getSequence());
		
		trackFX1Controller.init(0);
		trackFX1Controller.registerObserver(audioPlayerController);

		trackFX2Controller.init(1);
		trackFX2Controller.registerObserver(audioPlayerController);

		trackFX3Controller.init(2);
		trackFX3Controller.registerObserver(audioPlayerController);

		trackFX4Controller.init(3);
		trackFX4Controller.registerObserver(audioPlayerController);
		
		calibrationController.init(this);
		calibrationController.registerObserver(openCVEditorController);

		transportController.init();
		transportController.registerObserver(audioPlayerController);
		transportController.registerObserver(openCVEditorController);
		transportController.registerObserver(openCVSequencerController);
		
		Runnable sequenceGrabber = new Runnable() {
			
			@Override
			public void run()
			{
				audioPlayerController.setSequence(openCVEditorController.getSequence());
			}
		};

		this.timer = Executors.newSingleThreadScheduledExecutor();
		this.timer.scheduleAtFixedRate(sequenceGrabber, 0, 500, TimeUnit.MILLISECONDS);
	}
	
	public int[][] getSequence(){
		return openCVSequencerController.getSequence();
	}
	
	public Scalar[] fetchColorMinVals(){
		return openCVEditorController.getMinVals();
	}

	public Scalar[] fetchColorMaxVals(){
		return openCVEditorController.getMaxVals();
	}
}
