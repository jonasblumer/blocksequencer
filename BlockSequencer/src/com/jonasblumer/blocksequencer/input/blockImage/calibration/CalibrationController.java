package com.jonasblumer.blocksequencer.input.blockImage.calibration;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.jonasblumer.blocksequencer.helpers.EventTypes;
import com.jonasblumer.blocksequencer.helpers.JsonIO;
import com.jonasblumer.blocksequencer.helpers.Observer;
import com.jonasblumer.blocksequencer.helpers.Subject;
import com.jonasblumer.blocksequencer.main.MainController;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Slider;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class CalibrationController implements Subject {
	
	@FXML private Slider bckgrndHueStart, bckgrndHueStop, bckgrndSatStart, bckgrndSatStop, bckgrndValStart, bckgrndValStop, colorHueStart, colorHueStop, colorSatStart, colorSatStop, colorValStart, colorValStop;
	@FXML private ChoiceBox<String> colorSelect;
	@FXML private Pane colorIndicator;
	@FXML private Button saveBtn, loadBtn;
	
	private MainController main;

	private ArrayList<Observer> observers = new ArrayList<Observer>();

	public void init(MainController main){
		this.main = main;
		colorIndicator.setBackground(new Background(new BackgroundFill(Color.YELLOW, CornerRadii.EMPTY, Insets.EMPTY)));
		/*
		 * background sliders
		 */
		
		bckgrndHueStart.valueProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.COLOR_HUE_START, bckgrndHueStart.getValue(), 6);
		});
		bckgrndHueStop.valueProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.COLOR_HUE_STOP, bckgrndHueStop.getValue(), 6);
		});	
		
		bckgrndSatStart.valueProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.COLOR_SAT_START, bckgrndSatStart.getValue(), 6);
		});
		bckgrndSatStop.valueProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.COLOR_SAT_STOP, bckgrndSatStop.getValue(), 6);
		});
		
		bckgrndValStart.valueProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.COLOR_VAL_START, bckgrndValStart.getValue(), 6);
		});
		bckgrndValStop.valueProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.COLOR_VAL_STOP, bckgrndValStop.getValue(), 6);
		});
		
		/*
		 * color sliders
		 */
		colorHueStart.valueProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.COLOR_HUE_START, colorHueStart.getValue(), Integer.parseInt(colorSelect.getValue()));
		});
		colorHueStop.valueProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.COLOR_HUE_STOP, colorHueStop.getValue(), Integer.parseInt(colorSelect.getValue()));
		});	
		
		colorSatStart.valueProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.COLOR_SAT_START, colorSatStart.getValue(), Integer.parseInt(colorSelect.getValue()));
		});
		colorSatStop.valueProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.COLOR_SAT_STOP, colorSatStop.getValue(), Integer.parseInt(colorSelect.getValue()));
		});
		
		colorValStart.valueProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.COLOR_VAL_START, colorValStart.getValue(), Integer.parseInt(colorSelect.getValue()));
		});
		colorValStop.valueProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.COLOR_VAL_STOP, colorValStop.getValue(), Integer.parseInt(colorSelect.getValue()));
		});
		
		/*
		 * color select dropdown
		 */
		colorSelect.valueProperty().addListener((observe, oldValue, newValue) -> {
			updateSliderView((int) Integer.parseInt(colorSelect.getValue()));
			switch((int) Integer.parseInt(colorSelect.getValue())){
				case 0:
					colorIndicator.setBackground(new Background(new BackgroundFill(Color.YELLOW, CornerRadii.EMPTY, Insets.EMPTY)));
					break;
				case 1:
					colorIndicator.setBackground(new Background(new BackgroundFill(Color.PINK, CornerRadii.EMPTY, Insets.EMPTY)));
					break;
				case 2:
					colorIndicator.setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY)));
					break;
				case 3:
					colorIndicator.setBackground(new Background(new BackgroundFill(Color.ORANGE, CornerRadii.EMPTY, Insets.EMPTY)));
					break;
				case 4:
					colorIndicator.setBackground(new Background(new BackgroundFill(Color.BLUE, CornerRadii.EMPTY, Insets.EMPTY)));
					break;
				case 5:
					colorIndicator.setBackground(new Background(new BackgroundFill(Color.BROWN, CornerRadii.EMPTY, Insets.EMPTY)));
					break;
			}
		});
		updateSliderView(0);
		setColorSliderVals(0);
	}
	
	@FXML private void save(){
		JSONObject obj = new JSONObject();
		
		JSONArray list = new JSONArray();
		
		for(int i = 0; i < 7; i++){
			JSONObject color = new JSONObject();
			color.put("sample", i);
			color.put("hueStart", main.fetchColorMinVals()[i].val[0]);
			color.put("hueStop", main.fetchColorMaxVals()[i].val[0]);
			color.put("saturationStart", main.fetchColorMinVals()[i].val[1]);
			color.put("saturationStop", main.fetchColorMaxVals()[i].val[1]);
			color.put("valueStart", main.fetchColorMinVals()[i].val[2]);
			color.put("valueStop", main.fetchColorMaxVals()[i].val[2]);
			list.add(color);
		}
		
		obj.put("colorValues", list);
		JsonIO.writeJSON(obj);

	}
	
	@FXML private void load(){
		Object data = JsonIO.readJSON();
	
		JSONObject jsonObject = (JSONObject) data;
		
		if(data == null){
			return;
		}
		
		// loop array
		JSONArray colorValues = (JSONArray) jsonObject.get("colorValues");
		Iterator<Object> iterator = colorValues.iterator();
		while (iterator.hasNext()) {
			
			JSONObject color = (JSONObject) iterator.next();
			int sample = (int)((long)color.get("sample"));
	
			notifyObservers(EventTypes.COLOR_HUE_START, (double)color.get("hueStart"), sample);
			notifyObservers(EventTypes.COLOR_HUE_STOP, (double)color.get("hueStop"), sample);
			
			notifyObservers(EventTypes.COLOR_SAT_START, (double)color.get("saturationStart"), sample);
			notifyObservers(EventTypes.COLOR_SAT_STOP, (double)color.get("saturationStop"), sample);
	
			notifyObservers(EventTypes.COLOR_VAL_START, (double)color.get("valueStart"), sample);
			notifyObservers(EventTypes.COLOR_VAL_STOP, (double)color.get("valueStop"), sample);
		}
		updateSliderView(Integer.parseInt(colorSelect.getValue()));
	}
	
	private void updateSliderView(int sample){
		colorHueStart.setValue(main.fetchColorMinVals()[sample].val[0]);
		colorHueStop.setValue(main.fetchColorMaxVals()[sample].val[0]);
		colorSatStart.setValue(main.fetchColorMinVals()[sample].val[1]);
		colorSatStop.setValue(main.fetchColorMaxVals()[sample].val[1]);
		colorValStart.setValue(main.fetchColorMinVals()[sample].val[2]);
		colorValStop.setValue(main.fetchColorMaxVals()[sample].val[2]);
		
		bckgrndHueStart.setValue(main.fetchColorMinVals()[6].val[0]);
		bckgrndHueStop.setValue(main.fetchColorMaxVals()[6].val[0]);
		bckgrndSatStart.setValue(main.fetchColorMinVals()[6].val[1]);
		bckgrndSatStop.setValue(main.fetchColorMaxVals()[6].val[1]);
		bckgrndValStart.setValue(main.fetchColorMinVals()[6].val[2]);
		bckgrndValStop.setValue(main.fetchColorMaxVals()[6].val[2]);
	}
	
	private void setColorSliderVals(int sample){
		
		notifyObservers(EventTypes.COLOR_HUE_START, colorHueStart.getValue(), sample);
		notifyObservers(EventTypes.COLOR_HUE_STOP, colorHueStop.getValue(), sample);
		
		notifyObservers(EventTypes.COLOR_SAT_START, colorSatStart.getValue(), sample);
		notifyObservers(EventTypes.COLOR_SAT_STOP, colorSatStop.getValue(), sample);

		notifyObservers(EventTypes.COLOR_VAL_START, colorValStart.getValue(), sample);
		notifyObservers(EventTypes.COLOR_VAL_STOP, colorValStop.getValue(), sample);
	}

	@Override
	public void registerObserver(Observer observer) {
		observers.add(observer);
		
	}

	@Override
	public void removeObserver(Observer observer) {
		observers.remove(observer);
		
	}

	@Override
	public void notifyObservers(EventTypes updateType, double value, int target) {
		for (Observer ob : observers) {
			 ob.update(updateType, value, target);
		 }
	}
	
}
