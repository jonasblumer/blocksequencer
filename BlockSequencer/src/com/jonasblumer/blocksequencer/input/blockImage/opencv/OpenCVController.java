package com.jonasblumer.blocksequencer.input.blockImage.opencv;

import java.io.ByteArrayInputStream;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

import com.jonasblumer.blocksequencer.helpers.EventTypes;
import com.jonasblumer.blocksequencer.helpers.Observer;
import com.jonasblumer.blocksequencer.main.MainController;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public abstract class OpenCVController implements Observer{
	
	protected OpenCVModel model;
	
	@FXML private ImageView img;
	
	// a timer for acquiring the video stream
	private ScheduledExecutorService timer;
	// the OpenCV object that realizes the video capture
	protected VideoCapture capture = new VideoCapture();
	// a flag to change the button behavior
	private boolean cameraActive = false;
	
	private static final int CAM_RES_WIDTH = 960;
	private static final int CAM_RES_HEIGHT = 480;
	
	public void init(){
		model = new OpenCVModel();
	}
	
	public void play()
	{	
		if (!this.cameraActive)
		{
			// start the video capture
			this.capture.open(0);
			

			// is the video stream available?
			if (this.capture.isOpened())
			{
				
				this.capture.set(Videoio.CV_CAP_PROP_FRAME_WIDTH, CAM_RES_WIDTH);
				this.capture.set(Videoio.CV_CAP_PROP_FRAME_HEIGHT, CAM_RES_HEIGHT);
				
				this.cameraActive = true;
				
				// grab a frame every 100 ms (10 frames/sec)
				Runnable frameGrabber = new Runnable() {
					
					@Override
					public void run()
					{
						Image imageToShow = grabFrame();
						img.setImage(imageToShow);
					}
				};

				this.timer = Executors.newSingleThreadScheduledExecutor();
				this.timer.scheduleAtFixedRate(frameGrabber, 0, 100, TimeUnit.MILLISECONDS);
				
			}
			else
			{
				// log the error
				System.err.println("Impossible to open the camera connection...");
			}
		}
		else
		{
			// the camera is not active at this point
			this.cameraActive = false;
			
			// stop the timer
			try
			{
				timer.shutdown();
				timer.awaitTermination(33, TimeUnit.MILLISECONDS);
			}
			catch (InterruptedException e)
			{
				// log the exception
				System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
			}
			
			// release the camera
			capture.release();
			// clean the frame
			img.setImage(null);
		}
	}
	
	
	/**
	 * Get a frame from the opened video stream (if any)
	 * 
	 * @return the {@link Image} to show
	 */
	protected abstract Image grabFrame();
	
	/**
	 * Convert a Mat object (OpenCV) in the corresponding Image for JavaFX
	 * 
	 * @param frame
	 *            the {@link Mat} representing the current frame
	 * @return the {@link Image} to show
	 */
	protected Image mat2Image(Mat frame)
	{
		// create a temporary buffer
		MatOfByte buffer = new MatOfByte();
		// encode the frame in the buffer
		Imgcodecs.imencode(".png", frame, buffer);
		// build and return an Image created from the image encoded in the
		// buffer
		return new Image(new ByteArrayInputStream(buffer.toArray()));
	}
	
	public int[][] getSequence(){
		return model.getSequence();
	}
	
	@Override
	public void update(EventTypes updateType, double value, int target) {
		switch(updateType){
		case PLAY:
			play();
			break;
		case STOP:
			play();
			break;
		}
	}

}
