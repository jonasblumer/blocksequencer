package com.jonasblumer.blocksequencer.input.blockImage.opencv;

import java.io.ByteArrayInputStream;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

import com.jonasblumer.blocksequencer.main.MainController;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class OpenCVSequencerController extends OpenCVController{
	
	protected Image grabFrame()
	{
		// init everything
		Image imageToShow = null;
		Mat frame = new Mat();
		// check if the capture is open
		if (super.capture.isOpened())
		{
			try
			{
				// read the current frame
				this.capture.read(frame);
				Core.flip(frame, frame, 0);
			
				// if the frame is not empty, process it
				if (!frame.empty())
				{
					// convert the Mat object (OpenCV) to Image (JavaFX)
					imageToShow = mat2Image(frame);
				}
				
			}
			catch (Exception e)
			{
				// log the error
				System.err.println("Exception during the image elaboration: " + e);
			}
		}else{
			System.out.println("ohh no, no opened");
		}
		return imageToShow;
	}

	
	
}
