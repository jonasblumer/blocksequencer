package com.jonasblumer.blocksequencer.input.blockImage.opencv;

public class OpenCVModel {
	
	private int[][] sequence;
	
	public OpenCVModel(){

		sequence = new int[4][8];
		initEmptySequence();
	}
	
	private void initEmptySequence(){
		/*
		 * fill array with dummy beat feat
		 */
		for(int i = 0; i < sequence.length; i++){
			for(int j = 0; j < sequence[i].length; j++){
				sequence[i][j] = 6;
			}
		}
	}

	public int[][] getSequence() {
		return sequence;
	}
	
	public void setStep(int track, int step, int sample){
		sequence[track][step] = sample;
	}

}
