package com.jonasblumer.blocksequencer.input.blockImage.opencv;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import com.jonasblumer.blocksequencer.helpers.EventTypes;

import javafx.scene.image.Image;

public class OpenCVEditorController extends OpenCVController{
	
	private Scalar[] minVals, maxVals;
	
	private int falseMatchCount;
	
	public OpenCVEditorController(){
		
		minVals = new Scalar[7];
		maxVals = new Scalar[7];
		
		falseMatchCount = 0;
		
		for(int i = 0; i < 7; i++){
			minVals[i] = new Scalar(0, 0, 0);
			maxVals[i] = new Scalar(0, 0, 0);
		}
	}

	protected Image grabFrame()
	{
		// init everything
		Image imageToShow = null;
		Mat frame = new Mat();
		
		// check if the capture is open
		if (super.capture.isOpened())
		{
			try
			{
				// read the current frame
				this.capture.read(frame);
				Core.flip(frame, frame, 0);

				// if the frame is not empty, process it
				if (!frame.empty())
				{
					// init
					Mat blurredImage = new Mat();
					Mat hsvImage = new Mat();
					
					// remove some noise
					Imgproc.blur(frame, blurredImage, new Size(21, 21));

					// convert the frame to HSV
					Imgproc.cvtColor(blurredImage, hsvImage, Imgproc.COLOR_BGR2HSV);
					
				 	checkMatchForColor(hsvImage);
				 	
					imageToShow = mat2Image(hsvImage);
				}
				
			}
			catch (Exception e)
			{
				// log the error
				System.err.println("Exception during the image elaboration: " + e);
			}
		}else{
			System.out.println("ohh no, no opened");
		}
		return imageToShow;
	}
	
	private void checkMatchForColor(Mat hsvImage){
		
		double roiWidth = hsvImage.width() / 8;
		double roiHeight = hsvImage.height() / 4;
		Mat morphOutput = null;
		Mat crop = null;
		Mat mask = new Mat();
		
		for(int row = 0; row < 4; row++){
			for(int column = 0; column < 8; column ++){
				// define roi
				Point rectTopLeft = new Point(column * (int)roiWidth, row * (int)roiHeight);
				Point rectBottomRight = new Point(column * (int)roiWidth + (int)roiWidth, row * (int)roiHeight + (int)roiHeight);
				Imgproc.rectangle(hsvImage, rectTopLeft, rectBottomRight, new Scalar(255, 0, 0));
				
				
				// empty roi check
				Scalar minBackgroundValues = new Scalar(minVals[6].val[0], minVals[6].val[1], minVals[6].val[2]);
				Scalar maxBackgroundValues = new Scalar(maxVals[6].val[0], maxVals[6].val[1], maxVals[6].val[2]);
				Core.inRange(hsvImage, minBackgroundValues, maxBackgroundValues, mask);

				// morphout is the mask set over the whole thing int the size and pos of ROI
				morphOutput = getCroppedMask(mask, rectTopLeft, rectBottomRight);
				
				// check if covered
				if(isCovered(morphOutput)){
					// color contain check
					Imgproc.circle(hsvImage, new Point(rectTopLeft.x+10, rectTopLeft.y+10), 10, new Scalar(200, 200, 200));
					falseMatchCount = 0;
					for(int k = 0; k < 6; k++){
						
						Core.inRange(hsvImage, minVals[k], maxVals[k], mask);
						morphOutput = getCroppedMask(mask, rectTopLeft, rectBottomRight);
						
						
						crop = hsvImage.submat(new Rect(rectTopLeft, rectBottomRight));
						this.findAndDrawMatches(morphOutput, crop, row, column, k);
					}
				}else{
					model.setStep(row, column, 6);
				}
			}
		}
	}
	
	private Mat getCroppedMask(Mat mask, Point rectTopLeft, Point rectBottomRight){
		Mat morphOutput = new Mat();
		Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(11, 11));
		Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5, 5));
		Mat cropMask = mask.submat(new Rect(rectTopLeft, rectBottomRight));
		
		Imgproc.erode(cropMask, morphOutput, erodeElement);
		Imgproc.erode(cropMask, morphOutput, erodeElement);
		
		Imgproc.dilate(cropMask, morphOutput, dilateElement);
		Imgproc.dilate(cropMask, morphOutput, dilateElement);
		return morphOutput;
	}
	
	private boolean isCovered(Mat mask){
		List<MatOfPoint> contours = new ArrayList<>();
		Mat hierarchy = new Mat();
		
		// find contours
		Imgproc.findContours(mask, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_TC89_L1);
		
		// if any contour exist...
		if (hierarchy.size().height > 0 && hierarchy.size().width > 0)
		{
			return false;
		}
		return true;
		
	}
	
	private Mat findAndDrawMatches(Mat maskedImage, Mat frame, int row, int column, int sample)
	{
		// init
		List<MatOfPoint> contours = new ArrayList<>();
		Mat hierarchy = new Mat();
		
		// find contours
		Imgproc.findContours(maskedImage, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_TC89_L1);
		
		// if any contour exist...
		if (hierarchy.size().height > 0 && hierarchy.size().width > 0)
		{
			//update beat in model
			model.setStep(row, column, sample);
			// for each contour, display it in blue
			Scalar color = null;
			for (int idx = 0; idx >= 0; idx = (int) hierarchy.get(0, idx)[0]){
				switch(sample){
				case 0:
					color = new Scalar(0, 255, 255); // yellow
					break;
				case 1:
					color = new Scalar(147,20,255); // pink
					break;
				case 2:
					color = new Scalar(50,205,50); // green
					break;
				case 3:
					color = new Scalar(80,127,255); // orange
					break;
				case 4:
					color = new Scalar(255,0,0); // blue
					break;
				case 5:
					color = new Scalar(0,25,100); // brown
					break;
				}
				Imgproc.drawContours(frame, contours, idx, color, -1);
			}
		}else{
				falseMatchCount++;
				if(falseMatchCount >= 6){
					falseMatchCount = 0;
					model.setStep(row, column, 6);
				}
		}
		return frame;
	}
	
	public Scalar[] getMinVals(){
		return minVals;
	}
	
	public Scalar[] getMaxVals(){
		return maxVals;
	}


	@Override
	public void update(EventTypes updateType, double value, int target) {
		switch(updateType){
		case COLOR_HUE_START:
			this.minVals[target].val[0] = value;
			break;
		case COLOR_HUE_STOP:
			this.maxVals[target].val[0] = value;
			break;
		case COLOR_SAT_START:
			this.minVals[target].val[1] = value;
			break;
		case COLOR_SAT_STOP:
			this.maxVals[target].val[1] = value;
			break;
		case COLOR_VAL_START:
			this.minVals[target].val[2] = value;
			break;
		case COLOR_VAL_STOP:
			this.maxVals[target].val[2] = value;
			break;
		case PLAY:
			super.play();
			break;
		case STOP:
			super.play();
			break;
		}
		
	}
}
