package com.jonasblumer.blocksequencer.output.audio.transport;

import java.util.ArrayList;

import com.jonasblumer.blocksequencer.helpers.EventTypes;
import com.jonasblumer.blocksequencer.helpers.Observer;
import com.jonasblumer.blocksequencer.helpers.Subject;
import com.jonasblumer.blocksequencer.main.MainController;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;

public class TransportController implements Subject{
	
	private ArrayList<Observer> observers = new ArrayList<Observer>();

	@FXML private ToggleButton isPlayingButton;
	@FXML private Slider bpmSlider;
	
	
	@FXML private void isPlayingToggle(ActionEvent event){
		if(isPlayingButton.selectedProperty().getValue()){
			notifyObservers(EventTypes.PLAY, -1, -1);
		}else{
			notifyObservers(EventTypes.STOP, -1, -1);
		}
	}
	
	
	public void init() {
		bpmSlider.valueChangingProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.BPM, bpmSlider.getValue(), -1);
		});		
	}

	@Override
	public void registerObserver(Observer observer) {
		observers.add(observer);
		
	}

	@Override
	public void removeObserver(Observer observer) {
		observers.remove(observer);
		
	}

	@Override
	public void notifyObservers(EventTypes updateType, double value, int target) {
		for (Observer ob : observers) {
			 ob.update(updateType, value, target);
		 }
	}
	
}
