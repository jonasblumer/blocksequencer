package com.jonasblumer.blocksequencer.output.audio.sample;

import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.core.UGenChain;
import net.beadsproject.beads.data.SampleManager;
import net.beadsproject.beads.ugens.Gain;
import net.beadsproject.beads.ugens.Reverb;
import net.beadsproject.beads.ugens.SamplePlayer;

public class Sample extends UGenChain{
	
	private SamplePlayer samplePlayer;
	private Gain cleanGain, wetGain;
	private Reverb reverb;
	private AudioContext ac;

	public Sample(AudioContext ac, int ins, int outs) {
		super(ac, ins, outs);
		this.ac = ac;
		reverb = new Reverb(ac, 2);
		reverb.setSize(0);
		reverb.setDamping(.2f);
		cleanGain = new Gain(ac, 2, .7f);
		wetGain = new Gain(ac, 2, 1f);
		
	}
	
	public void setSample(int sample){
		samplePlayer = new SamplePlayer(ac, SampleManager.sample("resources/samples/sample"+sample+".aif"));
		samplePlayer.setToEnd();
		/*
		 * don't unload sample after playback
		 */
		samplePlayer.setKillOnEnd(false);
		reverb.addInput(samplePlayer);
		/*
		 * as reverb only outputs reverberated signal, we connect 
		 * samplePlayer and reverb to output
		 */
		wetGain.addInput(reverb);
		cleanGain.addInput(samplePlayer);
		ac.out.addInput(wetGain);
		ac.out.addInput(cleanGain);
	}

	public SamplePlayer getSamplePlayer() {
		return samplePlayer;
	}
	
	public Reverb getReverb(){
		return reverb;
	}
	
	public Gain getCleanGain(){
		return cleanGain;
	}
	public Gain getWetGain(){
		return wetGain;
	}

}
