package com.jonasblumer.blocksequencer.output.audio.audioplayer;

import com.jonasblumer.blocksequencer.main.MainController;
import com.jonasblumer.blocksequencer.output.audio.sample.Sample;
import com.jonasblumer.blocksequencer.output.audio.track.Track;

import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.core.Bead;
import net.beadsproject.beads.ugens.Clock;
import net.beadsproject.beads.ugens.Envelope;

public class AudioPlayerModel {
	
	private AudioContext ac;
	private Clock beatClock;
	private Bead beatGen;
	private Sample[] sample;
	private int[][] sequence;
	private Track[] track;
	
	public AudioPlayerModel(){
		
		ac = new AudioContext();
		
		sample = new Sample[6];
		sequence = new int[4][8];
		
		/*
		 * init tracks
		 */
		track = new Track[4];
		for(int i = 0; i < track.length; i++){
			track[i] = new Track();
		}
		
		beatClock = new Clock(ac, 0);
		setBpm(2000);
		beatClock.setClick(false);
		beatClock.setTicksPerBeat(4);
		ac.out.addDependent(beatClock);
		
		/*
		 * load sounds
		 */
		for(int i = 0; i < sample.length; i++){
			sample[i] = new Sample(ac, 2, 2);
			sample[i].setSample(i);
		}
		
		beatGen = new Bead () {
			  
			  public void messageReceived(Bead message){
				  
				  for(int i = 0; i < 4; i++){

					  if(sequence[i][track[i].getCurrentStep()] < 6){
						  /*
						   * check track of sound and set reverb for sound accordingly
						   */
						  sample[sequence[i][track[i].getCurrentStep()]].getReverb().setSize(track[i].getReverbVal());
						  sample[sequence[i][track[i].getCurrentStep()]].getReverb().setDamping(track[i].getDampVal());
						  sample[sequence[i][track[i].getCurrentStep()]].getCleanGain().setGain(track[i].getDryVolume());
						  sample[sequence[i][track[i].getCurrentStep()]].getWetGain().setGain(track[i].getWetVolume());
						  sample[sequence[i][track[i].getCurrentStep()]].getSamplePlayer().reTrigger();
					  }
					  /*
					   * increment or reset
					   */
					  if(track[i].getCurrentStep() < track[i].getStepMax() - 1){
						  track[i].setCurrentStep(track[i].getCurrentStep() + 1);
					  }else{
						  track[i].setCurrentStep(0);
					  }
				  }
			  }
		};
		
		beatClock.addMessageListener(beatGen);
	}
	
	/*
	 * GETTERS / SETTERS
	 */
	
	protected void isPlaying(boolean isPlaying){
		if(isPlaying){
			
			/*
			 * beatGen is added to listen to beatClock. when beatGen receives a message, it plays a sound.
			 * it gets a message every tick of the beatclock
			 */
			beatGen.pause(false);
			ac.start();
		}else{
			beatGen.pause(true);
			ac.stop();
		}
	}
	
	
	protected int[][] getSequence() {
		return sequence;
	}

	protected void setSequence(int[][] sequence) {
		this.sequence = sequence;
	}

	protected int getCurrentStep(int trackNum) {
		return track[trackNum].getCurrentStep();
	}

	protected void resetCurrentSteps() {
		for(int i = 0; i < 4; i++){
			track[i].setCurrentStep(0);
		}
	}

	protected void setBpm(float speed) {
		beatClock.setIntervalEnvelope( new Envelope(ac,speed));
	}

	protected void setReverb(float amount, int trackNum) {
		track[trackNum].setReverbVal(amount);
	}
	
	protected void setDamping(float amount, int trackNum) {
		track[trackNum].setDampVal(amount);
	}

	protected void setStepMax(int value, int trackNum) {
		track[trackNum].setStepMax(value);
	}
	
	protected void setWetVolume(float volume, int trackNum){
		track[trackNum].setWetVolume(volume);
	}
	protected void setDryVolume(float volume, int trackNum){
		track[trackNum].setDryVolume(volume);
	}
}
