package com.jonasblumer.blocksequencer.output.audio.audioplayer;

import com.jonasblumer.blocksequencer.helpers.EventTypes;
import com.jonasblumer.blocksequencer.helpers.Observer;
import com.jonasblumer.blocksequencer.main.MainController;

public class AudioPlayerController implements Observer{
	private AudioPlayerModel model;
	
	public AudioPlayerController(){
		model = new AudioPlayerModel();
	}
	
	
	public void play(){
		model.resetCurrentSteps();
		model.isPlaying(true);
		/*
		 * debut print sequence
		 */
		for(int i = 0; i < model.getSequence().length; i++){
			for(int j = 0; j < model.getSequence()[i].length; j++){
				System.out.print(model.getSequence()[i][j] + " ");
			}
			System.out.println("");
		}
	}
	
	public void stop(){
		model.isPlaying(false);
	}
	
	public void resetCurrentSteps(){
		model.resetCurrentSteps();
	}

	public void setSequence(int sequence[][]){
		model.setSequence(sequence);
	}

	@Override
	public void update(EventTypes updateType, double value, int target) {
		switch(updateType){
		case DRY_VOL:
			model.setDryVolume((float) value, target);
			break;
		case WET_VOL:
			model.setWetVolume((float) value, target);
			break;
		case STEP_COUNT:
			model.setStepMax((int)value, target);
			break;
		case REVERB:
			model.setReverb((float)value, target);
			break;
		case DAMPING:
			model.setDamping((float)value, target);
			break;
		case BPM:
			float speed = (float) (60000 / value * 2);
			model.setBpm(speed);
			break;
		case PLAY:
			play();
			break;
		case STOP:
			stop();
			break;
		}
		
	}
	
}
