package com.jonasblumer.blocksequencer.output.audio.track;

import java.util.ArrayList;

import com.jonasblumer.blocksequencer.helpers.EventTypes;
import com.jonasblumer.blocksequencer.helpers.Observer;
import com.jonasblumer.blocksequencer.helpers.Subject;
import com.jonasblumer.blocksequencer.main.MainController;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;

public class TrackFXController implements Subject{
	@FXML private Slider reverb, damp, stepCount, wetVol, dryVol;
	@FXML private Label label;
	
	private int trackNum;
	
	private ArrayList<Observer> observers = new ArrayList<Observer>();
	
	public void init(int trackNum) {
		this.trackNum = trackNum;
		
		label.setText("Track " + (trackNum+1));
		
		notifyObservers(EventTypes.DRY_VOL, dryVol.getValue(), trackNum);
		
		reverb.valueProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.REVERB, reverb.getValue(), trackNum);
		});	
		damp.valueProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.DAMPING, damp.getValue(), trackNum);
		});	
		stepCount.valueProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.STEP_COUNT, stepCount.getValue(), trackNum);
		});
		wetVol.valueProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.WET_VOL, wetVol.getValue(), trackNum);
		});
		dryVol.valueProperty().addListener((observe, oldValue, newValue) -> {
			notifyObservers(EventTypes.DRY_VOL, dryVol.getValue(), trackNum);
		});
	}

	@Override
	public void registerObserver(Observer observer) {
		observers.add(observer);
		
	}

	@Override
	public void removeObserver(Observer observer) {
		observers.remove(observer);
		
	}

	@Override
	public void notifyObservers(EventTypes updateType, double value, int target) {
		for (Observer ob : observers) {
			 ob.update(updateType, value, target);
		 }
	}
}
