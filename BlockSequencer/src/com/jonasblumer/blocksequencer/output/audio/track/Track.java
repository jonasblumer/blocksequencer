package com.jonasblumer.blocksequencer.output.audio.track;

public class Track {
	private float reverbVal;
	private float dampVal;
	private int currentStep;
	private int stepMax;
	private float wetVolume;
	private float dryVolume;

	public Track(){
		setReverbVal(0);
		setDampVal(0);
		setCurrentStep(0);
		setStepMax(8);
		setWetVolume(.5f);
		setDryVolume(.5f);
	}

	public float getReverbVal() {
		return reverbVal;
	}

	public void setReverbVal(float reverbVal) {
		this.reverbVal = reverbVal;
	}

	public float getDampVal() {
		return dampVal;
	}

	public void setDampVal(float dampVal) {
		this.dampVal = dampVal;
	}

	public int getCurrentStep() {
		return currentStep;
	}

	public void setCurrentStep(int currentStep) {
		this.currentStep = currentStep;
	}

	public int getStepMax() {
		return stepMax;
	}

	public void setStepMax(int stepMax) {
		this.stepMax = stepMax;
	}

	public float getWetVolume() {
		return wetVolume;
	}

	public void setWetVolume(float wetVolume) {
		this.wetVolume = wetVolume;
	}

	public float getDryVolume() {
		return dryVolume;
	}

	public void setDryVolume(float dryVolume) {
		this.dryVolume = dryVolume;
	}
	
}
